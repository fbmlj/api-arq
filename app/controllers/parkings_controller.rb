class ParkingsController < ApplicationController
  before_action :set_parking, only: [:show, :update, :destroy]

  # GET /parkings
  def index
    @parkings = Parking.all

    render json: @parkings
  end

  # GET /parkings/1
  def show
    render json: @parking
  end

  # POST /parkings
  def create
    @parking = Parking.new(parking_params)

    if @parking.save
      render json: @parking, status: :created, location: @parking
    else
      render json: @parking.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /parkings/1
  def update
    if @parking.update(parking_params)
      render json: @parking
    else
      render json: @parking.errors, status: :unprocessable_entity
    end
  end

  # DELETE /parkings/1
  def destroy
    @parking.destroy
  end

  def country
    @parkings = Parking.all.order("state")
    
    @list = Array.new
    @list.push(Parking.first)
    @parkings.each do |park|
      if @list[-1].state != park.state
        @list.push(park)
      end
    end
    
    
    render json: @list
  end

  def state
    @parkings = Parking.where(state: params[:state]).order("city")
    
    @list = Array.new
    @list.push(@parkings.first)
    @parkings.each do |park|
      if @list[-1].city != park.city
        @list.push(park)
      end
    end
    
    
    render json: @list
  end

  
  def city
    @parkings = Parking.where(city: params[:city]).order("district")
    
    @list = Array.new
    @list.push(@parkings.first)
    @parkings.each do |park|
      if @list[-1].district != park.district
        @list.push(park)
      end
    end
    
    
    render json: @list
  end



  def district
    @parkings = Parking.where(district: params[:district])
    render json: @parkings 
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_parking
      @parking = Parking.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def parking_params
      params.require(:parking).permit(:name, :empty, :street,:number, :cep, :city, :district, :state)
    end


end
