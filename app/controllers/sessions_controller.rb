class SessionsController < ApplicationController
  before_action :set_session, only: [:show, :update, :destroy]

  # GET /sessions
  def index
    @sessions = Session.all

    render json: @sessions
  end

  # GET /sessions/1
  def show
    render json: @session
  end

  # POST /sessions
  def create
    @user = User.find_by(email: params[:email])
    @session = Session.new
    if !@user
      render json: "email não encontrado ", status: :unprocessable_entity
    else
      Session.where(user_id: @user).destroy_all
      if params[:password]==@user.password
        @session.user_id = @user.id    
        @session.token = generate_token
        if @session.save
          render json: @session, status: :created, location: @session
        else
          render json: @session.errors, status: :unprocessable_entity
        end
      else
        render json: "senha errado", status: :unprocessable_entity
      end
    end
  end

  # PATCH/PUT /sessions/1
  def update
    if @session.update(session_params)
      render json: @session
    else
      render json: @session.errors, status: :unprocessable_entity
    end
  end
  # DELETE /sessions/1

  def destroy
    puts @user
    @session.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_session
      @session = Session.find_by!(user_id: params[:user_id])
       'erro'
    end

    # Only allow a trusted parameter "white list" through.
    def session_params
      params.require(:session).permit(:user_id, :token)
      
    end

    def generate_token
      loop do
        token = SecureRandom.hex(10)
        break token unless Session.where(token: token).exists?
      end
    end
end
