Rails.application.routes.draw do
  get 'parkings_state/:state', to: "parkings#state"
  get 'parkings_district/:district', to: "parkings#district"
  get 'parkings_country/', to: "parkings#country"
  get 'parkings_city/:city', to: "parkings#city"
  resources :sessions, except: [:show, :destroy]
  get 'sessions/:user_id' => "sessions#show"
  delete 'sessions/:user_id' => "sessions#destroy"
  resources :users
  resources :parkings
  resources :sessions, only: [:create, :destroy]
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
