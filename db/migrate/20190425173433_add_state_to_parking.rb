class AddStateToParking < ActiveRecord::Migration[5.2]
  def change
    add_column :parkings, :state, :string
  end
end
