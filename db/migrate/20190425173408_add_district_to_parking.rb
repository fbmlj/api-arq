class AddDistrictToParking < ActiveRecord::Migration[5.2]
  def change
    add_column :parkings, :district, :string
  end
end
