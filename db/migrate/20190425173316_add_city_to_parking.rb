class AddCityToParking < ActiveRecord::Migration[5.2]
  def change
    add_column :parkings, :city, :string
  end
end
