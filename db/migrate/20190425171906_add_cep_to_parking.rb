class AddCepToParking < ActiveRecord::Migration[5.2]
  def change
    add_column :parkings, :cep, :string
  end
end
