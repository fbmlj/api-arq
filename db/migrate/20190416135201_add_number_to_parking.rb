class AddNumberToParking < ActiveRecord::Migration[5.2]
  def change
    add_column :parkings, :number, :integer
  end
end
