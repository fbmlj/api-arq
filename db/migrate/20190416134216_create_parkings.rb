class CreateParkings < ActiveRecord::Migration[5.2]
  def change
    create_table :parkings do |t|
      t.string :name
      t.integer :empty
      t.string :street

      t.timestamps
    end
  end
end
